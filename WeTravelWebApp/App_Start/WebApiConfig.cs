﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using WeTravelWebApp.Providers;
using WeTravelWebApp.Areas.HelpPage;
using System.Web;
using System.Web.Http.Cors;

namespace WeTravelWebApp
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // My custom filter
            config.Filters.Add(new WebAPIActionFilterAttribute());
            
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Combine with Project property "Build" setting, generate Web-based API spec. Browse URL /Help to check the spec.
            // See http://www.asp.net/web-api/overview/getting-started-with-aspnet-web-api/creating-api-help-pages
            config.SetDocumentationProvider(new XmlDocumentationProvider(HttpContext.Current.Server.MapPath("~/App_Data/WeTravelWebApp.WebAPISpec.xml")));
        }
    }
}
