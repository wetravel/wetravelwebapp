// Load libraries
import angular from 'angular';
import _ from 'lodash';

import 'angular-animate';
import 'angular-aria';
import 'angular-route';
import 'angular-material';
import 'angular-cookies';
import 'angular-messages';
import 'angular-simple-logger';
import 'angular-google-maps';
import 'angular-moment';
import 'angular-ui-tinymce';
import 'tinymce';
import 'jquery';
import 'angular-signalr-hub';
import 'signalr';

import AppController from 'src/AppController';

import Register from 'src/accounts/components/register/Register';
import ChangeUserInfo from 'src/accounts/components/change-user-info/ChangeUserInfo';
import Login from 'src/accounts/components/login/Login';
import Logout from 'src/accounts/components/logout/Logout';

import ListTravelIdeas from 'src/travel-ideas/components/list/ListTravelIdeas';
import CreateTravelIdea from 'src/travel-ideas/components/create/CreateTravelIdea';
import UpdateTravelIdea from 'src/travel-ideas/components/update/UpdateTravelIdea';
import OpenTravelIdea from 'src/travel-ideas/components/open/OpenTravelIdea';


export default angular.module( 'app', [ 'ngRoute', 'ngMaterial', 'ngCookies', 'ngAnimate', 'ngMessages', 'angularMoment', 'ui.tinymce', 'SignalR', 'uiGmapgoogle-maps' ] )
    .config(($mdIconProvider, $mdThemingProvider, $routeProvider) => {
        // Register the user `avatar` icons
        $mdIconProvider
            .defaultIconSet("./assets/svg/avatars.svg", 128)
            .icon("menu", "./assets/svg/menu.svg", 24)
            .icon("account_circle", "./assets/svg/ic_account_circle_white_24px.svg", 24)
            .icon("add", "./assets/svg/ic_add_white_24px.svg", 24)
            .icon("open", "./assets/svg/ic_open_in_browser_black_24px.svg", 24)
            .icon("edit", "./assets/svg/ic_edit_black_24px.svg", 24)
            .icon("delete", "./assets/svg/ic_delete_black_24px.svg", 24)
            .icon("back", "./assets/svg/ic_arrow_back_black_24px.svg", 24)
            .icon("title", "./assets/svg/ic_title_black_24px.svg", 24)
            .icon("author", "./assets/svg/ic_account_circle_black_24px.svg", 24)
            .icon("email", "./assets/svg/ic_email_black_24px.svg", 24)
            .icon("location", "./assets/svg/ic_location_on_black_24px.svg", 24)
            .icon("date", "./assets/svg/ic_date_range_black_24px.svg", 24)
            .icon("created_time", "./assets/svg/ic_access_time_black_24px.svg", 24)
            .icon("comment", "./assets/svg/ic_comment_black_24px.svg", 24)
            .icon("key", "./assets/svg/ic_key_black_24px.svg", 24)
            .icon("person", "./assets/svg/ic_person_black_24px.svg", 24)
            .icon("search", "./assets/svg/ic_search_black_24px.svg", 24)
            .icon("hotel", "./assets/svg/ic_hotel_black_24px.svg", 24)
            .icon("star", "./assets/svg/ic_star_black_24px.svg", 24)
            .icon("price", "./assets/svg/ic_attach_money_black_24px.svg", 24)
            .icon("share", "./assets/svg/share.svg", 24)
            .icon("google_plus", "./assets/svg/google_plus.svg", 24)
            .icon("hangouts", "./assets/svg/hangouts.svg", 24)
            .icon("twitter", "./assets/svg/twitter.svg", 24)
            .icon("phone", "./assets/svg/phone.svg", 24);

        $mdThemingProvider.theme('default')
            .primaryPalette('blue')
            .accentPalette('orange');

        $routeProvider
            .when('/register', Register.config)
            .when('/change-user-info', ChangeUserInfo.config)
            .when('/login', Login.config)
            .when('/logout', Logout.config)
            .when('/list-travel-ideas', ListTravelIdeas.config)
            .when('/create-travel-idea', CreateTravelIdea.config)
            .when('/update-travel-idea/:id', UpdateTravelIdea.config)
            .when('/open-travel-idea/:id', OpenTravelIdea.config);

        tinyMCE.baseURL = 'jspm_packages/npm/tinymce@4.5.0';

    })
    .config(function(uiGmapGoogleMapApiProvider) {
        uiGmapGoogleMapApiProvider.configure({
            key: 'AIzaSyD7f8UgnHonaiNkpWlO4e_rIHsG7w2JG3k',
            v: '3.20', //defaults to latest 3.X anyhow
            libraries: 'weather,geometry,visualization'
        });
    })
    .filter('leftHalf', function() {
        return function(arr) {
            return arr.slice(0, Math.ceil(arr.length / 2));
        };
    })
    .filter('rightHalf', function() {
        return function(arr) {
            return arr.slice(Math.ceil(arr.length / 2), arr.length);
        };
    })
    .controller('AppController', AppController)
    .run(['$rootScope', function($rootScope) {
        $rootScope.$on("$routeChangeSuccess", function () {
            $('#content').scrollTop(0);
        });
    }])
    .factory('jQuery', [
        '$window',
        function ($window) {
            return $window.jQuery;
        }
    ]);
