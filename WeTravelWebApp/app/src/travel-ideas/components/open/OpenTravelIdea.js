export default {
    name : 'openTravelIdea',
    config : {
        parent: angular.element(document. getElementById('content')),
        templateUrl      : 'src/travel-ideas/components/open/OpenTravelIdea.html',
        controllerAs     : '$ctrl',
        bindToController : true,
        disableBackdrop  : true,
        disableParentScroll : false,
        controller       : [ '$rootScope', '$mdBottomSheet', '$mdToast', '$log', '$scope', '$http', '$cookies', '$httpParamSerializerJQLike', '$sce', '$routeParams', '$anchorScroll', '$window', '$location', class OpenTravelIdeaController {

            /**
             * Constructor
             */
            constructor($rootScope, $mdBottomSheet, $mdToast, $log, $scope, $http, $cookies, $httpParamSerializerJQLike, $sce, $routeParams, $anchorScroll, $window, $location) {
                var self = this;
                this.$rootScope = $rootScope;
                this.$mdBottomSheet = $mdBottomSheet;
                this.$mdToast = $mdToast;
                this.$log = $log;
                this.$scope = $scope;
                this.$http = $http;
                this.$cookies = $cookies;
                this.$httpParamSerializerJQLike = $httpParamSerializerJQLike;
                this.$sce = $sce;
                this.$routeParams = $routeParams;
                this.$anchorScroll = $anchorScroll;
                this.$window = $window;
                this.$location = $location;

                this.id = $routeParams.id;
                this.inputs = ['Comment'];
                this.idea = {
                    Id: '',
                    Title: '',
                    Destination: '',
                    From: '',
                    To: '',
                    Content: '',
                    ContentText: '',
                    ContentRTF: '',
                    Tags: [],
                    Author: {},
                    CreatedBy: '',
                    CreatedAt: '',
                    UpdatedBy: '',
                    UpdatedAt: ''
                };
                this.hotels = [];

                self.isIdeaLoaded = false;
                self.isCommentLoaded = false;

                $scope.newCommentArrive = function(comment){
                    var scopeSelf = this;
                    if (this.$ctrl.id != comment.IdeaId){
                        return;
                    }


                    if (!this.$ctrl.comments){
                        this.$ctrl.comments = [];
                    }
                    this.$ctrl.comments.unshift(comment);
                    this.$ctrl.comments.forEach(function (item) {
                        item.Content = scopeSelf.$ctrl.$sce.trustAsHtml(item.ContentText);
                    });
                };

                $scope.map = {
                    center: {
                        latitude: 0,
                        longitude: 0
                    },
                    zoom: 8
                };
                $scope.marker = {
                    id: 0
                }


            }

            /**
             * Load Travel Idea
             */
            loadIdea(isTextOnly) {
                var self = this;
                var params = {};
                if (isTextOnly) {
                    params.isTextOnly = true;
                }
                self.$rootScope.progress = true;

                self.$http({
                    method: 'GET',
                    url: self.$rootScope.baseUrl + 'api/TravelIdea/' + self.$routeParams.id,
                    headers: {'Authorization': 'bearer ' + self.$cookies.get('token')},
                    params: params,
                    paramSerializer: '$httpParamSerializerJQLike'
                })
                .success(function (data) {
                    self.idea = data.Payload;
                    self.idea.Content = self.$sce.trustAsHtml(self.idea.ContentRTF ? self.idea.ContentRTF : self.idea.ContentText);

					//self.$http.jsonp('http://getcitydetails.geobytes.com/GetCityDetails?callback=JSON_CALLBACK&fqcn=' + self.idea.Destination)
                    self.$http.jsonp(self.$rootScope.baseUrl + 'api/Geobytes/GetCityDetails?callback=JSON_CALLBACK&fqcn=' + self.idea.Destination)
                    .success(function (data) {
                        if (data.geobyteslatitude && data.geobyteslongitude) {
                            self.$scope.map = {
                                center: {
                                    latitude: data.geobyteslatitude,
                                    longitude: data.geobyteslongitude
                                },
                                zoom: 8
                            };
                            self.$scope.marker = {
                                id: 0,
                                coords: {
                                    latitude: data.geobyteslatitude,
                                    longitude: data.geobyteslongitude
                                },
                                options: {
                                    draggable: false,
                                    labelContent: 'lat: ' + data.geobyteslatitude + ' ' + 'lon: ' + data.geobyteslongitude,
                                    labelAnchor: '100 0',
                                    labelClass: 'marker-labels'
                                },
                                events: {
                                    click: function (marker, eventName, args) {
                                        self.$scope.infoWindow.windowOptions.visible = !self.$scope.infoWindow.windowOptions.visible;
                                    }
                                }
                            }
                            self.$scope.infoWindow = {
                                windowOptions: {
                                    visible: false
                                },
                                locationName: data.geobytescity
                            }
                            self.$http.jsonp('https://api.hotwire.com/v1/search/hotel?apikey=tbqn4ssh7z5ddcrwxmgz7ate&format=jsonp&callback=JSON_CALLBACK&dest=' + data.geobyteslatitude + ',' + data.geobyteslongitude + '&startdate=' + self.toUSDateString(self.idea.From) + '&enddate=' + self.toUSDateString(self.idea.To) + '&rooms=1&children=0&adults=1')
                            .success(function (data) {
                                if (data.Result) {
                                    self.hotels = data.Result;
                                }
                                if (data.MetaData && data.MetaData.HotelMetaData && data.MetaData.HotelMetaData.Neighborhoods) {
                                    self.neighborhoods = data.MetaData.HotelMetaData.Neighborhoods;
                                }
                                if (self.hotels.length < 1) {
                                    self.$mdToast.showSimple('Could not find available hotels.');
                                }
                                self.hotels.forEach(function (hotel) {
                                    switch (hotel.LodgingTypeCode) {
                                        case 'H':
                                            hotel.LodgingType = 'Hotel';
                                            break;
                                        case 'C':
                                            hotel.LodgingType = 'Condo';
                                            break;
                                        case 'A':
                                            hotel.LodgingType = 'All-inclusive resort';
                                            break;
                                    }
                                    hotel.Neighborhood = self.$window.$.grep(self.neighborhoods, function(item) { return item.Id == hotel.NeighborhoodId; })[0];
                                    if (!hotel.SavingsPercentage) {
                                        hotel.SavingsPercentage = 0;
                                    }
                                });
                                self.isIdeaLoaded = true;
                                if (self.isCommentLoaded) {
                                    self.$rootScope.progress = false;
                                }
                            })
                            .error(function (data, status) {
                                console.log(self.hotels);
                                self.$mdToast.showSimple('Could not find available hotels.');
                                self.isIdeaLoaded = true;
                                if (self.isCommentLoaded) {
                                    self.$rootScope.progress = false;
                                }
                            });
                        } else {
                            self.$scope.map = {
                                center: {
                                    latitude: 22.283001,
                                    longitude: 114.150002
                                },
                                zoom: 1
                            };
                            self.$mdToast.showSimple('Could not find the destination in the map.');
                        }
                    });
                })
                .error(function (data, status) {
                    if (data.Message) {
                        self.$mdToast.showSimple(data.Message);
                    }
                });
            }

            deleteIdea(id) {
                var self = this;
                self.$rootScope.progress = true;

                self.$http({
                    method: 'DELETE',
                    url: self.$rootScope.baseUrl + 'api/TravelIdea',
                    headers: {'Authorization': 'bearer ' + self.$cookies.get('token')},
                    params: {id: id},
                    paramSerializer: '$httpParamSerializerJQLike'
                })
                .success(function (data) {
                    self.isIdeaLoaded = true;
                    if (self.isCommentLoaded) {
                        self.$rootScope.progress = false;
                    }
                    self.$mdToast.showSimple('Delete a travel idea successfully.');
                    self.$window.location.href = '#list-travel-ideas';
                })
                .error(function (data, status) {
                    if (data.Message) {
                        self.$mdToast.showSimple(data.Message);
                    }
                    self.isIdeaLoaded = true;
                    if (self.isCommentLoaded) {
                        self.$rootScope.progress = false;
                    }
                });
            }

            back() {
                history.go(-1);
            }

            loadComments(isTextOnly, newItemsAfter, tags) {
                var self = this;
                var params = {};
                if (isTextOnly) {
                    params.isTextOnly = true;
                }
                if (newItemsAfter) {
                    params.newItemsAfter = newItemsAfter;
                }
                if (tags) {
                    params.tags = tags;
                }
                self.$rootScope.progress = true;

                self.$http({
                    method: 'GET',
                    url: self.$rootScope.baseUrl + 'api/TravelIdea/' + self.$routeParams.id + '/Comments',
                    headers: {'Authorization': 'bearer ' + self.$cookies.get('token')},
                    params: params,
                    paramSerializer: '$httpParamSerializerJQLike'
                })
                .success(function (data) {
                    if (newItemsAfter) {
                        data.Payload.forEach(function (item) {
                            self.comments.unshift(item);
                        });
                    } else {
                        self.comments = data.Payload;
                    }
                    self.comments.forEach(function (item) {
                        item.Content = self.$sce.trustAsHtml(item.ContentText);
                    });
                    self.isCommentLoaded = true;
                    if (self.isIdeaLoaded) {
                        self.$rootScope.progress = false;
                    }
                })
                .error(function (data, status) {
                    if (data.Message) {
                        self.$mdToast.showSimple(data.Message);
                    }
                    self.isCommentLoaded = true;
                    if (self.isIdeaLoaded) {
                        self.$rootScope.progress = false;
                    }
                });
            }

            addComment() {
                var self = this;
                self.$rootScope.progress = true;

                self.$scope.formErrors = {
                    Comment: ''
                };

                if (self.$scope.Comment) {
                    self.$http({
                        method: 'POST',
                        url: self.$rootScope.baseUrl + 'api/TravelIdea/' + self.id + '/Comments',
                        headers: {'Authorization': 'bearer ' + self.$cookies.get('token')},
                        data: JSON.stringify({
                            IdeaId: self.id,
                            ContentText: self.$scope.Comment
                        })
                    })
                    .success(function (data) {
                        self.inputs.forEach(function (item) {
                            if (self.$scope.commentTravelIdeaForm[item] && self.$scope.commentTravelIdeaForm[item].$setValidity) {
                                self.$scope.Comment = ' ';
                                self.$scope.commentTravelIdeaForm[item].$setValidity('server', true);
                            }
                        });
                        // Deprecated code after using SignalR
                        //if (self.comments[0]) {
                        //    self.loadComments(false, self.comments[0].CreatedAt);
                        //} else {
                        //    self.loadComments();
                        //}

                        self.$rootScope.progress = false;
                        self.$mdToast.showSimple('Added a comment successfully.');
                        self.$anchorScroll('top-item');
                    })
                    .error(function (data, status) {
                        if (data.Message) {
                            self.inputs.forEach(function (item) {
                                if (self.$scope.commentTravelIdeaForm[item] && self.$scope.commentTravelIdeaForm[item].$setValidity) {
                                    self.$scope.commentTravelIdeaForm[item].$setValidity('server', true);
                                    if (data.Message.toLowerCase().includes(item.toLowerCase().substring(0, 4))) {
                                        self.$scope.formErrors[item] = data.Message;
                                        self.$scope.commentTravelIdeaForm[item].$setValidity('server', false);
                                    }
                                }
                            });
                            self.$mdToast.showSimple(data.Message);
                        }
                        self.$rootScope.progress = false;
                    });
                }

            }

            getUserName() {
                return this.$cookies.get('username');
            }

            toUSDateString(dateISO) {
                return dateISO.substring(5,7) + '/' + dateISO.substring(8,10) + '/' + dateISO.substring(0,4);
            }

            redirect(url) {
                this.$window.location.href = url;
            }
        } ]
    }
};


