export default {
    name : 'createTravelIdea',
    config : {
        parent: angular.element(document. getElementById('content')),
        templateUrl         : 'src/travel-ideas/components/create/CreateTravelIdea.html',
        controllerAs        : '$ctrl',
        bindToController    : true,
        disableBackdrop     : true,
        disableParentScroll : false,
        controller          : [ '$rootScope', '$mdBottomSheet', '$mdToast', '$log', '$scope', '$http', '$cookies', '$window', '$q', class CreateTravelIdeaController {

            /**
             * Constructor
             *
             * @param $mdBottomSheet
             * @param $log
             */
            constructor($rootScope, $mdBottomSheet, $mdToast, $log, $scope, $http, $cookies, $window, $q) {
                var self = this;
                this.$rootScope = $rootScope;
                this.$mdBottomSheet = $mdBottomSheet;
                this.$mdToast = $mdToast;
                this.$log = $log;
                this.$scope = $scope;
                this.$http = $http;
                this.$cookies = $cookies;
                this.$window = $window;
                this.$q = $q;

                this.$scope.Tags = [];
                this.inputs = ['Title', 'Destination', 'From', 'To', 'ContentRTF'];
                this.$scope.tinymceOptions = {
                    init_instance_callback: function (editor) {
                        self.$scope.editor = editor;
                    }
                }

                this.selectedDestination = null;
                this.searchDestinationText = null;
            }

            /**
             * Create Travel Idea
             */
            createTravelIdea() {
                var self = this;
                self.$rootScope.progress = true;
                self.$scope.formErrors = {
                    Title: '',
                    Destination: '',
                    From: '',
                    To: '',
                    ContentRTF: '',
                    Tags: ''
                };

                if (self.$scope.Title && self.selectedDestination && self.$scope.From && self.$scope.To && self.$scope.ContentRTF) {
                    self.$http({
                        method: 'POST',
                        url: self.$rootScope.baseUrl + 'api/TravelIdea',
                        headers: {'Authorization': 'bearer ' + self.$cookies.get('token')},
                        data: JSON.stringify({
                            Title: self.$scope.Title,
                            Destination: self.selectedDestination,
                            From: self.$scope.From,
                            To: self.$scope.To,
                            ContentRTF: self.$scope.ContentRTF,
                            ContentText: self.$scope.editor.getContent({ format: 'text' }),
                            Tags: self.$scope.Tags
                        })
                    })
                    .success(function (data) {
                        self.inputs.forEach(function (item) {
                            self.$scope.createTravelIdeaForm[item].$setValidity('server', true);
                        });
                        self.$rootScope.progress = false;
                        self.$mdToast.showSimple('Created a travel idea successfully.');
                        self.$window.location.href = '#list-travel-ideas';

                    })
                    .error(function (data, status) {
                        if (data.Message) {
                            self.inputs.forEach(function (item) {
                                if (self.$scope.createTravelIdeaForm[item]) {
                                    self.$scope.createTravelIdeaForm[item].$setValidity('server', true);
                                    if (data.Message.toLowerCase().includes(item.toLowerCase().substring(0, 4))) {
                                        self.$scope.formErrors[item] = data.Message;
                                        self.$scope.createTravelIdeaForm[item].$setValidity('server', false);
                                    }
                                }
                            });
                            self.$mdToast.showSimple(data.Message);
                        }
                        self.$rootScope.progress = false;
                    });
                }
            }

            searchDestination() {
                var self = this;
                var lowercaseQuery = angular.lowercase(self.searchDestinationText);
                if (lowercaseQuery.length > 2) {
                    var deferred = self.$q.defer();
					
					//self.$http.jsonp('http://gd.geobytes.com/AutoCompleteCity?callback=JSON_CALLBACK&q=' + lowercaseQuery)
                    self.$http.jsonp(self.$rootScope.baseUrl + 'api/Geobytes/AutoCompleteCity?callback=JSON_CALLBACK&q=' + lowercaseQuery)
                        .success(function (data) {
                            deferred.resolve(data);
                        });
                    return deferred.promise;
                } else {
                    return [];
                }
            }
        } ]
    }
};


