export default {
    name : 'changeUserInfo',
    config : {
        parent: angular.element(document. getElementById('content')),
        templateUrl         : 'src/accounts/components/change-user-info/ChangeUserInfo.html',
        controllerAs        : '$ctrl',
        bindToController    : true,
        disableBackdrop     : true,
        disableParentScroll : false,
        controller          : [ '$rootScope', '$mdBottomSheet', '$mdToast', '$log', '$scope', '$http', '$cookies', '$window', class ChangePasswordController {

            /**
             * Constructor
             */
            constructor($rootScope, $mdBottomSheet, $mdToast, $log, $scope, $http, $cookies, $window) {
                this.$rootScope = $rootScope;
                this.$mdBottomSheet = $mdBottomSheet;
                this.$mdToast = $mdToast;
                this.$log = $log;
                this.$scope = $scope;
                this.$http = $http;
                this.$cookies = $cookies;
                this.$window = $window;

                this.inputs = ['FirstName', 'LastName', 'Email', 'OldPassword', 'NewPassword', 'ConfirmPassword'];
            }


            loadUserInfo() {
                var self = this;
                self.$rootScope.progress = true;
                self.$http({
                    method: 'GET',
                    url: self.$rootScope.baseUrl + 'api/Account/UserInfo',
                    headers: {'Authorization': 'bearer ' + self.$cookies.get('token')},
                })
                .success(function (data) {
                    self.$scope.FirstName = data.Payload.FirstName;
                    self.$scope.LastName = data.Payload.LastName;
                    self.$scope.Email = data.Payload.Email;
                    self.$rootScope.progress = false;
                })
                .error(function (data, status) {
                    if (data.Message) {
                        self.$mdToast.showSimple(data.Message);
                    }
                    self.$rootScope.progress = false;
                });
            }

            /**
             * Change User Information
             */
            changeUserInfo() {
                var self = this;
                self.$rootScope.progress = true;
                self.$scope.formErrors = {
                    FirstName: '',
                    LastName: '',
                    Email: '',
                    OldPassword: '',
                    NewPassword: '',
                    ConfirmPassword: ''
                };
                if (self.$scope.FirstName && self.$scope.LastName && self.$scope.Email) {
                    self.$http({
                        method: 'POST',
                        url: self.$rootScope.baseUrl + 'api/Account/UserInfo',
                        headers: {'Authorization': 'bearer ' + self.$cookies.get('token')},
                        data: JSON.stringify({
                            FirstName: self.$scope.FirstName,
                            LastName: self.$scope.LastName,
                            Email: self.$scope.Email,
                            OldPassword: self.$scope.OldPassword,
                            NewPassword: self.$scope.NewPassword,
                            ConfirmPassword: self.$scope.ConfirmPassword
                        })
                    })
                    .success(function (data) {
                        self.inputs.forEach(function (item) {
                            self.$scope.changeUserInfoForm[item].$setValidity('server', true);
                        });
                        self.$rootScope.progress = false;
                        self.$mdToast.showSimple('Changed user information successfully.');
                        self.$window.location.href = '#list-travel-ideas';
                    })
                    .error(function (data, status) {
                        if (data.Message) {
                            self.inputs.forEach(function (item) {
                                self.$scope.changeUserInfoForm[item].$setValidity('server', true);
                                if (data.Message.toLowerCase().includes(item.toLowerCase().substring(0, 4))) {
                                    self.$scope.formErrors[item] = data.Message;
                                    self.$scope.changeUserInfoForm[item].$setValidity('server', false);
                                }
                            });
                            self.$mdToast.showSimple(data.Message);
                        }
                        self.$rootScope.progress = false;
                    });
                }
            }
        } ]
    }
};


