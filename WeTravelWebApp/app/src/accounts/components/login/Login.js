export default {
    name : 'login',
    config : {
        parent: angular.element(document. getElementById('content')),
        templateUrl         : 'src/accounts/components/login/Login.html',
        controllerAs        : '$ctrl',
        bindToController    : true,
        disableBackdrop     : true,
        disableParentScroll : false,
        controller          : [ '$rootScope', '$mdBottomSheet', '$mdToast', '$log', '$scope', '$http', '$cookies', '$window', class LoginController {

            /**
             * Constructor
             */
            constructor($rootScope, $mdBottomSheet, $mdToast, $log, $scope, $http, $cookies, $window) {
                this.$rootScope = $rootScope;
                this.$mdBottomSheet = $mdBottomSheet;
                this.$mdToast = $mdToast;
                this.$log = $log;
                this.$scope = $scope;
                this.$http = $http;
                this.$cookies = $cookies;
                this.$window = $window;

                this.inputs = ['Email', 'Password'];
            }

            /**
             * Login
             */
            login() {
                var self = this;
                self.$rootScope.progress = true;
                self.$scope.formErrors = {
                    Email: '',
                    Password: ''
                };
                if (self.$scope.Email && self.$scope.Password) {
                    self.$http({
                        method: 'POST',
                        url: self.$rootScope.baseUrl + 'Token',
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: {
                            grant_type: 'password',
                            Username: self.$scope.Email,
                            Password: self.$scope.Password
                        },
                        transformRequest: function(values) {
                            var data = [];
                            for (var param in values) {
                                data.push(encodeURIComponent(param) + '=' + encodeURIComponent(values[param]))
                            }
                            return data.join('&');
                        }
                    })
                    .success(function (data) {
                        self.inputs.forEach(function (item) {
                            self.$scope.loginForm[item].$setValidity('server', true);
                        });
                        self.$cookies.put('username', data.userName);
                        self.$cookies.put('token', data.access_token);
                        self.$rootScope.progress = false;
                        self.$mdToast.showSimple('Logged in successfully.');
                        self.$window.location.href = '#list-travel-ideas';
                    })
                    .error(function (data, status) {
                        if (data.Message) {
                            self.inputs.forEach(function (item) {
                                self.$scope.loginForm[item].$setValidity('server', true);
                                if (data.Message.toLowerCase().includes(item.toLowerCase().substring(0,4))) {
                                    self.$scope.formErrors[item] = data.Message;
                                    self.$scope.loginForm[item].$setValidity('server', false);
                                }
                            });
                            self.$mdToast.showSimple(data.Message);
                        }
                        self.$rootScope.progress = false;
                    });
                }
            }
        } ]
    }
};


