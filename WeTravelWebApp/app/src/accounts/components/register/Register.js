export default {
    name : 'register',
    config : {
        parent: angular.element(document. getElementById('content')),
        templateUrl      : 'src/accounts/components/register/Register.html',
        controllerAs     : '$ctrl',
        bindToController : true,
        disableBackdrop  : true,
        disableParentScroll : false,
        controller       : [ '$rootScope', '$mdBottomSheet', '$mdToast', '$log', '$scope', '$http', '$cookies', '$window', class RegisterController {

            /**
             * Constructor
             */
            constructor($rootScope, $mdBottomSheet, $mdToast, $log, $scope, $http, $cookies, $window) {
                this.$rootScope = $rootScope;
                this.$mdBottomSheet = $mdBottomSheet;
                this.$mdToast = $mdToast;
                this.$log = $log;
                this.$scope = $scope;
                this.$http = $http;
                this.$cookies = $cookies;
                this.$window = $window;

                this.inputs = ['FirstName', 'LastName', 'Email', 'Password', 'ConfirmPassword'];
            }

            /**
             * Register
             */
            register() {
                var self = this;
                self.$rootScope.progress = true;
                self.$scope.formErrors = {
                    FirstName: '',
                    LastName: '',
                    Email: '',
                    Password: '',
                    ConfirmPassword: ''
                };
                if (self.$scope.FirstName && self.$scope.LastName && self.$scope.Email &&
                    self.$scope.Password && self.$scope.ConfirmPassword) {
                    self.$http({
                        method: 'POST',
                        url: self.$rootScope.baseUrl + 'api/Account/Register',
                        data: JSON.stringify({
                            FirstName: self.$scope.FirstName,
                            LastName: self.$scope.LastName,
                            Email: self.$scope.Email,
                            Password: self.$scope.Password,
                            ConfirmPassword: self.$scope.ConfirmPassword
                        })
                    })
                    .success(function (data) {
                        self.$scope.registerForm.FirstName.$setValidity('server', true);
                        self.$scope.registerForm.LastName.$setValidity('server', true);
                        self.$scope.registerForm.Email.$setValidity('server', true);
                        self.$scope.registerForm.Password.$setValidity('server', true);
                        self.$scope.registerForm.ConfirmPassword.$setValidity('server', true);

                        self.$http({
                            method: 'POST',
                            url: self.$rootScope.baseUrl + 'Token',
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                            data: {
                                grant_type: 'password',
                                Username: self.$scope.Email,
                                Password: self.$scope.Password
                            },
                            transformRequest: function(values) {
                                var data = [];
                                for (var param in values) {
                                    data.push(encodeURIComponent(param) + '=' + encodeURIComponent(values[param]))
                                }
                                return data.join('&');
                            }
                        })
                        .success(function (data) {
                            self.inputs.forEach(function (item) {
                                self.$scope.registerForm[item].$setValidity('server', true);
                            });
                            self.$cookies.put('username', data.userName);
                            self.$cookies.put('token', data.access_token);
                            self.$rootScope.progress = false;
                            self.$mdToast.showSimple('Registered successfully.');
                            self.$window.location.href = '#list-travel-ideas';
                        })
                        .error(function (data) {
                            if (data.Message) {
                                self.$mdToast.showSimple(data.Message);
                            }
                            self.$rootScope.progress = false;
                        });
                    })
                    .error(function (data, status) {
                        if (data.Message) {
                            self.inputs.forEach(function (item) {
                                self.$scope.registerForm[item].$setValidity('server', true);
                                if (data.Message.toLowerCase().includes(item.toLowerCase().substring(0, 4))) {
                                    self.$scope.formErrors[item] = data.Message;
                                    self.$scope.registerForm[item].$setValidity('server', false);
                                }
                            });
                            if (data.Message.includes('Name') && data.Message.includes('is already taken.')) {
                                self.$scope.formErrors.Email = 'Email \'' + self.$scope.Email + '\' is already taken.';
                                self.$scope.registerForm.Email.$setValidity('server', false);
                            }

                            self.$mdToast.showSimple(data.Message);
                            self.$rootScope.progress = false;
                        }
                    });
                }
            }
        } ]
    }
};


