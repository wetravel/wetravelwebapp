/**
 * Main App Controller for WeTravel
 * @constructor
 */
function AppController($rootScope, $mdSidenav, $mdToast, $mdMedia, $cookies, $window, $scope, $anchorScroll) {
    var self = this;

    self.selected = null;
    self.$rootScope = $rootScope;
    self.$mdMedia = $mdMedia;
    self.$cookies = $cookies;
    self.$window = $window;
    self.$scope = $scope;
    self.$anchorScroll = $anchorScroll;

    self.init = init;
    self.toggleSideMenu = toggleSideMenu;
    self.logout = logout;
    self.isLoggedIn = isLoggedIn;

    // *********************************
    // Internal methods
    // *********************************

    function init () {
        $rootScope.$mdMedia = $mdMedia;
        $rootScope.baseUrl = 'https://wetravelwebapp.azurewebsites.net/';
        if (isLoggedIn()) {
            $window.location.href = '#list-travel-ideas';
        } else {
            $window.location.href = '#login';
        }
    }

    /**
    * jquery visible function to invoke $mdToast
    */
    $scope.showNewItemToast = function(msg) {
        $mdToast.show($mdToast.simple()
            .textContent(msg)
            .action('Go')
        ).then(function () {
            self.$anchorScroll('top-item');
        });
    };

    /**
     * Hide or Show the 'left' sideNav area
     */
    function toggleSideMenu() {
        $mdSidenav('left').toggle();
    }


    function logout() {
        if (self.$scope.OldPassword && self.$scope.NewPassword && self.$scope.ConfirmPassword) {
            self.$http({
                method: 'POST',
                url: self.$rootScope.baseUrl + 'api/Account/Logout',
                headers: {'Authorization': 'bearer ' + self.$cookies.get('token')}
            })
            .success(function (data) {
                self.$cookies.remove('username');
                self.$cookies.remove('token');
            })
            .error(function (data, status) {
                self.$cookies.remove('username');
                self.$cookies.remove('token');
                if (data.Message) {
                    self.$mdToast.showSimple(data.Message);
                }
            });
        }
    }

    /**
    * Select the current avatars
    * @param menuId
    */
    function isLoggedIn() {
        return self.$cookies.get('username');
    }

}

export default [ '$rootScope', '$mdSidenav', '$mdToast', '$mdMedia', '$cookies', '$window', '$scope', '$anchorScroll', AppController ];
