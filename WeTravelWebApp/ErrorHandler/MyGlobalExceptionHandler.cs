﻿using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using WeTravelWebApp.Exceptions;
using WeTravelWebApp.Models;

namespace WeTravelWebApp.ErrorHandler
{
    /// <summary>
    /// Reference to http://www.asp.net/web-api/overview/error-handling/web-api-global-error-handling
    /// Reference to http://www.asp.net/web-api/overview/error-handling/exception-handling
    /// </summary>
    public class MyGlobalExceptionHandler : System.Web.Http.ExceptionHandling.ExceptionHandler
    {

        public override void Handle(ExceptionHandlerContext context)
        {
            base.Handle(context);

            if (context.Exception.GetType() == typeof(DbEntityValidationException))
            {
                var ex = (DbEntityValidationException)context.Exception;

                // Wrap the result with jsend format for json
                context.Result = new JSendErrorResult
                {
                    Request = context.ExceptionContext.Request,
                    Content = String.Join(". ", ex.EntityValidationErrors.Select(x => String.Join(". ", x.ValidationErrors.Select(y => $"{y.PropertyName} - {y.ErrorMessage}")))),
                };
                return;
            }

            ICustomHttpActionResult contextResult;
            var firstAcceptedFormat = context.Request.Headers.Accept.FirstOrDefault();
            if (firstAcceptedFormat == null || firstAcceptedFormat.MediaType == "*/*" || firstAcceptedFormat.MediaType == "application/json")
            {
                // Wrap the result with jsend format for json
                contextResult = new JSendErrorResult
                {
                    Request = context.ExceptionContext.Request,
                    Content = context.Exception.Message,
                };
            }
            else
            {
                // Hide the original exception details from the outbound response, thus protecting against exposure of system structure
                contextResult = new TextPlainErrorResult
                {
                    Request = context.ExceptionContext.Request,
                    Content = context.Exception.Message,
                };
            }

            if (context.Exception is UnauthorizedAccessException)
            {
                contextResult.StatusCode = HttpStatusCode.Unauthorized;
            }
            else if (context.Exception is ICustomValidationException)
            {
                contextResult.StatusCode = HttpStatusCode.BadRequest;
            }

            context.Result = contextResult;

        }

        interface ICustomHttpActionResult : IHttpActionResult
        {
            HttpStatusCode StatusCode { get; set; }
        }

        class TextPlainErrorResult : ICustomHttpActionResult
        {
            public TextPlainErrorResult()
            {
                StatusCode = HttpStatusCode.InternalServerError;
            }
            public HttpRequestMessage Request { get; set; }
            public string Content { get; set; }
            public HttpStatusCode StatusCode { get; set; }

            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                HttpResponseMessage response = new HttpResponseMessage(StatusCode);
                response.Content = new StringContent(Content);
                response.RequestMessage = Request;
                return Task.FromResult(response);
            }
        }

        class JSendErrorResult : ICustomHttpActionResult
        {
            public JSendErrorResult()
            {
                StatusCode = HttpStatusCode.InternalServerError;
            }
            public HttpRequestMessage Request { get; set; }
            public string Content { get; set; }
            public HttpStatusCode StatusCode { get; set; }

            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                HttpResponseMessage response = new HttpResponseMessage(StatusCode);

                var jsendObj = new JSendResponse(JSendResponse.STATUS_ERROR, Content, null);
                response.Content = new ObjectContent(typeof(JSendResponse), jsendObj, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
                //response.Content = new StringContent(Content);
                response.RequestMessage = Request;
                return Task.FromResult(response);
            }
        }
        public override bool ShouldHandle(ExceptionHandlerContext context)
        {
            //return base.ShouldHandle(context);

            // Reference http://stackoverflow.com/a/24634485
            return true;
        }
    }
}
