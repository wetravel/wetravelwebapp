namespace WeTravelWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SchemaUpdate01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ApplicationUsers", "FirstName", c => c.String());
            AddColumn("dbo.ApplicationUsers", "LastName", c => c.String());
            AlterColumn("dbo.TravelIdeaComment", "ContentText", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TravelIdeaComment", "ContentText", c => c.String());
            DropColumn("dbo.ApplicationUsers", "LastName");
            DropColumn("dbo.ApplicationUsers", "FirstName");
        }
    }
}
