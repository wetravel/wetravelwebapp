namespace WeTravelWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TaggableArticles",
                c => new
                    {
                        TaggableArticleId = c.Guid(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(maxLength: 255),
                        CreatedAt = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 255),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.TaggableArticleId);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagKey = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.TagKey);
            
            CreateTable(
                "dbo.ApplicationUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.IdentityUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.IdentityUserLogins",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        LoginProvider = c.String(),
                        ProviderKey = c.String(),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.IdentityUserRoles",
                c => new
                    {
                        RoleId = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ApplicationUser_Id = c.String(maxLength: 128),
                        IdentityRole_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.RoleId, t.UserId })
                .ForeignKey("dbo.ApplicationUsers", t => t.ApplicationUser_Id)
                .ForeignKey("dbo.IdentityRoles", t => t.IdentityRole_Id)
                .Index(t => t.ApplicationUser_Id)
                .Index(t => t.IdentityRole_Id);
            
            CreateTable(
                "dbo.IdentityRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TagTaggableArticles",
                c => new
                    {
                        Tag_TagKey = c.String(nullable: false, maxLength: 100),
                        TaggableArticle_TaggableArticleId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_TagKey, t.TaggableArticle_TaggableArticleId })
                .ForeignKey("dbo.Tags", t => t.Tag_TagKey, cascadeDelete: true)
                .ForeignKey("dbo.TaggableArticles", t => t.TaggableArticle_TaggableArticleId, cascadeDelete: true)
                .Index(t => t.Tag_TagKey)
                .Index(t => t.TaggableArticle_TaggableArticleId);
            
            CreateTable(
                "dbo.TravelIdeaComment",
                c => new
                    {
                        TaggableArticleId = c.Guid(nullable: false),
                        Author_Id = c.String(maxLength: 128),
                        TravelIdea_TaggableArticleId = c.Guid(nullable: false),
                        Id = c.Guid(nullable: false),
                        ContentText = c.String(),
                    })
                .PrimaryKey(t => t.TaggableArticleId)
                .ForeignKey("dbo.TaggableArticles", t => t.TaggableArticleId)
                .ForeignKey("dbo.ApplicationUsers", t => t.Author_Id)
                .ForeignKey("dbo.TravelIdea", t => t.TravelIdea_TaggableArticleId)
                .Index(t => t.TaggableArticleId)
                .Index(t => t.Author_Id)
                .Index(t => t.TravelIdea_TaggableArticleId);
            
            CreateTable(
                "dbo.TravelIdea",
                c => new
                    {
                        TaggableArticleId = c.Guid(nullable: false),
                        Author_Id = c.String(maxLength: 128),
                        Id = c.Guid(nullable: false),
                        Title = c.String(maxLength: 100),
                        Destination = c.String(maxLength: 100),
                        From = c.DateTime(nullable: false),
                        To = c.DateTime(nullable: false),
                        ContentText = c.String(),
                        ContentRTF = c.String(),
                    })
                .PrimaryKey(t => t.TaggableArticleId)
                .ForeignKey("dbo.TaggableArticles", t => t.TaggableArticleId)
                .ForeignKey("dbo.ApplicationUsers", t => t.Author_Id)
                .Index(t => t.TaggableArticleId)
                .Index(t => t.Author_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TravelIdea", "Author_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.TravelIdea", "TaggableArticleId", "dbo.TaggableArticles");
            DropForeignKey("dbo.TravelIdeaComment", "TravelIdea_TaggableArticleId", "dbo.TravelIdea");
            DropForeignKey("dbo.TravelIdeaComment", "Author_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.TravelIdeaComment", "TaggableArticleId", "dbo.TaggableArticles");
            DropForeignKey("dbo.IdentityUserRoles", "IdentityRole_Id", "dbo.IdentityRoles");
            DropForeignKey("dbo.IdentityUserRoles", "ApplicationUser_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.IdentityUserLogins", "ApplicationUser_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.IdentityUserClaims", "ApplicationUser_Id", "dbo.ApplicationUsers");
            DropForeignKey("dbo.TagTaggableArticles", "TaggableArticle_TaggableArticleId", "dbo.TaggableArticles");
            DropForeignKey("dbo.TagTaggableArticles", "Tag_TagKey", "dbo.Tags");
            DropIndex("dbo.TravelIdea", new[] { "Author_Id" });
            DropIndex("dbo.TravelIdea", new[] { "TaggableArticleId" });
            DropIndex("dbo.TravelIdeaComment", new[] { "TravelIdea_TaggableArticleId" });
            DropIndex("dbo.TravelIdeaComment", new[] { "Author_Id" });
            DropIndex("dbo.TravelIdeaComment", new[] { "TaggableArticleId" });
            DropIndex("dbo.TagTaggableArticles", new[] { "TaggableArticle_TaggableArticleId" });
            DropIndex("dbo.TagTaggableArticles", new[] { "Tag_TagKey" });
            DropIndex("dbo.IdentityUserRoles", new[] { "IdentityRole_Id" });
            DropIndex("dbo.IdentityUserRoles", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.IdentityUserLogins", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.IdentityUserClaims", new[] { "ApplicationUser_Id" });
            DropTable("dbo.TravelIdea");
            DropTable("dbo.TravelIdeaComment");
            DropTable("dbo.TagTaggableArticles");
            DropTable("dbo.IdentityRoles");
            DropTable("dbo.IdentityUserRoles");
            DropTable("dbo.IdentityUserLogins");
            DropTable("dbo.IdentityUserClaims");
            DropTable("dbo.ApplicationUsers");
            DropTable("dbo.Tags");
            DropTable("dbo.TaggableArticles");
        }
    }
}
