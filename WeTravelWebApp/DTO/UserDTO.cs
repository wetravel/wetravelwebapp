﻿using System;
using System.Linq.Expressions;
using WeTravelWebApp.Models;

namespace WeTravelWebApp.DTO
{
    public class UserDTO
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class ApplicationUserMapper
    {
        public Expression<Func<ApplicationUser, UserDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<ApplicationUser, UserDTO>>)(p => new UserDTO()
                {
                    Id = p.Id,
                    UserName = p.UserName,
                    Email = p.Email,
                    FirstName = p.FirstName,
                    LastName = p.LastName,
                }));
            }
        }
        
    }
}
