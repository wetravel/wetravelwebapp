﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WeTravelWebApp.DTO.Infrastructure;
using WeTravelWebApp.Models;

namespace WeTravelWebApp.DTO
{
    public class TravelIdeaCommentDTO
    {
        public TravelIdeaCommentDTO()
        {
            this.Tags = new List<String>();
        }

        /// <summary>
        /// ID of the parent idea
        /// </summary>
        public string IdeaId { get; set; }
        /// <summary>
        /// ID of the comment itself
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Purely Text of submited content for tinymce. 
        /// To get Purely Text from Tinymce, see http://stackoverflow.com/questions/5288083/tinymce-get-plain-text
        /// </summary>
        [Required]
        public string ContentText { get; set; }
        /// <summary>
        /// Base64-encoded RTF content for tinymce
        /// </summary>
        public IEnumerable<string> Tags { get; set; }
        /// <summary>
        /// (Read-only) Author
        /// </summary>
        public UserDTO Author { get; set; }
        /// <summary>
        /// (Read-only) Audit Trail
        /// </summary>
        public string CreatedBy { get; set; }
        /// <summary>
        /// (Read-only) Audit Trail
        /// </summary>
        public DateTime? CreatedAt { get; set; }
        /// <summary>
        /// (Read-only) Audit Trail
        /// </summary>
        public string UpdatedBy { get; set; }
        /// <summary>
        /// (Read-only) Audit Trail
        /// </summary>
        public DateTime? UpdatedAt { get; set; }
    }

    public class TravelIdeaCommentMapper
    {
        //private TagMapper _tagMapper = new TagMapper();
        private ApplicationUserMapper _applicationUserMapper = new ApplicationUserMapper();
        public Expression<Func<TravelIdeaComment, TravelIdeaCommentDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<TravelIdeaComment, TravelIdeaCommentDTO>>)(p => new TravelIdeaCommentDTO()
                {
                    IdeaId = p.TravelIdea.Id.ToString(),
                    Id = p.Id.ToString(),
                    ContentText = p.ContentText,
                    Tags = p.Tags.Select(x => x.TagKey),//.AsQueryable().Select(this._tagMapper.SelectorExpression),
                    Author = p.Author == null ? null : new UserDTO()
                    {
                        Email = p.Author.Email,
                        Id = p.Author.Id,
                        UserName = p.Author.UserName,
                        FirstName = p.Author.FirstName,
                        LastName = p.Author.LastName,
                    },
                    CreatedBy = p.CreatedBy,
                    CreatedAt = p.CreatedAt,
                    UpdatedBy = p.UpdatedBy,
                    UpdatedAt = p.UpdatedAt,

                }));
            }
        }
        
    }
}
