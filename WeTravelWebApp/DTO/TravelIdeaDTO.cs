﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using WeTravelWebApp.DAL;
using WeTravelWebApp.Models;

namespace WeTravelWebApp.DTO
{
    public class TravelIdeaDTO
    {
        public TravelIdeaDTO()
        {
            Tags = new List<String>();
        }
        public string Id { get; set; }
        /// <summary>
        /// Title of the idea
        /// </summary>
        [Required]
        public string Title { get; set; }
        /// <summary>
        /// Destination, e.g. Hong Kong, Australia, etc.
        /// </summary>
        [Required]
        public string Destination { get; set; }
        /// <summary>
        /// From Date
        /// </summary>
        [Required]
        public DateTime From { get; set; }
        /// <summary>
        /// To Date
        /// </summary>
        [Required]
        public DateTime To { get; set; }
        /// <summary>
        /// Purely Text of submited content for tinymce. 
        /// To get Purely Text from Tinymce, see http://stackoverflow.com/questions/5288083/tinymce-get-plain-text
        /// </summary>
        [Required]
        public string ContentText { get; set; }
        /// <summary>
        /// Base64-encoded RTF content for tinymce
        /// </summary>
        public string ContentRTF { get; set; }
        /// <summary>
        /// List of tags for this idea
        /// </summary>
        public IEnumerable<string> Tags { get; set; }

        public int CommentCount { get; set; }

        /// <summary>
        /// (Read-only) Author
        /// </summary>
        public UserDTO Author { get; set; }
        /// <summary>
        /// (Read-only) Audit Trail
        /// </summary>
        public string CreatedBy { get; set; }
        /// <summary>
        /// (Read-only) Audit Trail
        /// </summary>
        public DateTime? CreatedAt { get; set; }
        /// <summary>
        /// (Read-only) Audit Trail
        /// </summary>
        public string UpdatedBy { get; set; }
        /// <summary>
        /// (Read-only) Audit Trail
        /// </summary>
        public DateTime? UpdatedAt { get; set; }
    }

    public class TravelIdeaMapper
    {
        //private TagMapper _tagMapper = new TagMapper();
        private ApplicationUserMapper _applicationUserMapper = new ApplicationUserMapper();
        public Expression<Func<TravelIdea, TravelIdeaDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<TravelIdea, TravelIdeaDTO>>)(p => new TravelIdeaDTO()
                {
                    Id = p.Id.ToString(),
                    Title = p.Title,
                    Destination = p.Destination,
                    From = p.From,
                    To = p.To,
                    ContentText = p.ContentText,
                    ContentRTF = p.ContentRTF,
                    Tags = p.Tags.Select(x => x.TagKey),//.AsQueryable().Select(this._tagMapper.SelectorExpression),
                    CommentCount = p.TravelIdeaComments.Count(),
                    Author = p.Author == null ? null : new UserDTO()
                    {
                        Email = p.Author.Email,
                        Id = p.Author.Id,
                        UserName = p.Author.UserName,
                        FirstName = p.Author.FirstName,
                        LastName = p.Author.LastName,
                    },
                    CreatedBy = p.CreatedBy,
                    CreatedAt = p.CreatedAt,
                    UpdatedBy = p.UpdatedBy,
                    UpdatedAt = p.UpdatedAt,
                }));
            }
        }

        /// <summary>
        /// Apply field mapping from dto to entity which is common for create and update
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="dto"></param>
        /// <param name="db"></param>
        public void CommonMapping(TravelIdea entity, TravelIdeaDTO dto, ApplicationDbContext db)
        {
            entity.Title = dto.Title;
            entity.ContentRTF = dto.ContentRTF;
            entity.ContentText = dto.ContentText;
            entity.Destination = dto.Destination;
            entity.From = dto.From;
            entity.To = dto.To;

            var distinctTags = dto.Tags.Distinct();
            var existingTags = (from tag in db.Tags
                                where distinctTags.Contains(tag.TagKey)
                                select tag).ToList();
            var tagsToBeRemoved = (from tag in entity.Tags
                                   where !distinctTags.Contains(tag.TagKey)
                                   select tag).ToList();
            var newTagKeys = distinctTags.Where(x => !existingTags.Any(y => y.TagKey == x));
            var newTags = newTagKeys.Select(x => new Tag() { TagKey = x });
            var finalTagList = new List<Tag>(existingTags);
            foreach(var finalTag in finalTagList)
            {
                finalTag.TaggableArticles.Add(entity);
            }
            foreach (var tagRemoved in tagsToBeRemoved)
            {
                tagRemoved.TaggableArticles.Remove(entity);
            }
            finalTagList.AddRange(newTags);
            entity.Tags = finalTagList;
        }
    }
}
