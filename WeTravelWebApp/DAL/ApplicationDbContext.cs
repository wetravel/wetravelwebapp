﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Linq;
using WeTravelWebApp.Models;

namespace WeTravelWebApp.DAL
{
    [DbConfigurationType(typeof(MyDbConfiguration))]
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() : base("DefaultConnection")
        {
        }

        public virtual DbSet<ApplicationUser> Users { get; set; }
        public virtual DbSet<Tag> Tags { get; set; }
        public virtual DbSet<TaggableArticle> TaggableArticles { get; set; }
        public virtual DbSet<TravelIdea> TravelIdeas { get; set; }
        public virtual DbSet<TravelIdeaComment> TravelIdeaComments { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //// Avoid from "The conversion of a datetime2 data type to a datetime data type resulted in an out-of-range value."
            //modelBuilder.Properties<System.DateTime>().Configure(c => c.HasColumnType("datetime2"));

            // To solve the error "EntityType 'IdentityUserLogin' has no key defined" when adding Unity IoC support on AspNET.Identity 2.2.x
            // See http://stackoverflow.com/a/34501673/4684232
            modelBuilder.Entity<IdentityUserLogin>().HasKey<string>(l => l.UserId);
            modelBuilder.Entity<IdentityRole>().HasKey<string>(r => r.Id);
            modelBuilder.Entity<IdentityUserRole>().HasKey(r => new { r.RoleId, r.UserId });

        }


        /// <summary>
        /// Save Changes with Audit-aware
        /// </summary>
        /// <param name="username">current user</param>
        /// <returns></returns>
        public int SaveChanges(string username)
        {
            var now = DateTime.Now;

            var addedAuditedEntities = ChangeTracker.Entries<IAuditedEntity>()
              .Where(p => p.State == EntityState.Added)
              .Select(p => p.Entity);

            var modifiedAuditedEntities = ChangeTracker.Entries<IAuditedEntity>()
              .Where(p => p.State == EntityState.Modified)
              .Select(p => p.Entity);

            foreach (var added in addedAuditedEntities)
            {
                added.CreatedBy = username;
                added.CreatedAt = now;
                added.UpdatedBy = username;
                added.UpdatedAt = now;
            }

            foreach (var modified in modifiedAuditedEntities)
            {
                modified.UpdatedBy = username;
                modified.UpdatedAt = now;
            }

            return base.SaveChanges();
        }

    }
}