﻿using System.Data.Entity;
using System.Data.Entity.SqlServer;

namespace WeTravelWebApp.DAL
{
    public class MyDbConfiguration : DbConfiguration
    {
        public MyDbConfiguration()
        {
            // Check https://msdn.microsoft.com/en-us/data/dn456835.aspx for selection
            SetExecutionStrategy("System.Data.SqlClient", () => new SqlAzureExecutionStrategy());
        }
    }
}