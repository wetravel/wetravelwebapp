﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WeTravelWebApp.Models
{
    public partial class FileEntity : AuditedEntity
    {
        public FileEntity() : base()
        {
            Id = Guid.NewGuid();
            TravelIdeas = new List<TravelIdea>();
        }

        public Guid Id { get; set; }

        [StringLength(255)]
        public string FileName { get; set; }

        [StringLength(100)]
        public string ContentType { get; set; }

        public byte[] Content { get; set; }

        public int SizeInKB { get; set; }

        public string Title { get; set; }

        public virtual ICollection<TravelIdea> TravelIdeas { get; set; }

    }
}