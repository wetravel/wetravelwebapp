﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeTravelWebApp.Models
{
    public interface IAuditedEntity
    {

        string CreatedBy { get; set; }

        DateTime? CreatedAt { get; set; }

        string UpdatedBy { get; set; }

        DateTime? UpdatedAt { get; set; }
    }
}
