﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WeTravelWebApp.Models
{
    public class Tag
    {
        public Tag() : base()
        {
            TaggableArticles = new List<TaggableArticle>();
        }

        [Key]
        [StringLength(100)]
        public string TagKey { get; set; }

        public virtual ICollection<TaggableArticle> TaggableArticles { get; set; }
    }
}