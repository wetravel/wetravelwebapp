﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WeTravelWebApp.Models
{
    [Table("TravelIdeaComment")]   // Explicitly define Table name for Table per Type (TPT). See https://weblogs.asp.net/manavi/inheritance-mapping-strategies-with-entity-framework-code-first-ctp5-part-2-table-per-type-tpt
    public class TravelIdeaComment : TaggableArticle
    {
        public TravelIdeaComment() : base()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        /// <summary>
        /// Text-based Content
        /// </summary>
        /// 
        [StringLength(255)]
        public string ContentText { get; set; }

        [Required]
        public virtual TravelIdea TravelIdea { get; set; }

        /// <summary>
        /// Author of this comment
        /// </summary>
        public virtual ApplicationUser Author { get; set; }
    }
}