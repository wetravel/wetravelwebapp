﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WeTravelWebApp.Models
{
    [Table("TravelIdea")]   // Explicitly define Table name for Table per Type (TPT). See https://weblogs.asp.net/manavi/inheritance-mapping-strategies-with-entity-framework-code-first-ctp5-part-2-table-per-type-tpt
    public class TravelIdea : TaggableArticle
    {
        public TravelIdea() : base()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        [StringLength(100)]
        public string Title { get; set; }
        [StringLength(100)]
        public string Destination { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        /// <summary>
        /// Text-based Content
        /// </summary>
        public string ContentText { get; set; }
        /// <summary>
        /// Text-based Content
        /// </summary>
        public string ContentRTF { get; set; }
        /// <summary>
        /// Author of this comment
        /// </summary>
        public virtual ApplicationUser Author { get; set; }

        /// <summary>
        /// Comments
        /// </summary>
        public virtual ICollection<TravelIdeaComment> TravelIdeaComments { get; set; }
    }
}