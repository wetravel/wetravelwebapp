﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WeTravelWebApp.Models
{
    public class TaggableArticle : AuditedEntity
    {
        public TaggableArticle() : base()
        {
            TaggableArticleId = Guid.NewGuid();
            Tags = new List<Tag>();
        }

        [Key]
        public Guid TaggableArticleId { get; set; }
        public bool IsDeleted { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
    }
}