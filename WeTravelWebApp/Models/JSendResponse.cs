﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeTravelWebApp.Models
{
    public abstract class JSendResponseBase
    {
        public const string ARUGMENT_STATUS = "JSendStatus";
        public const string ARUGMENT_MSG = "JSendMsg";

        public const string STATUS_OK = "ok";
        public const string STATUS_WARN = "warn";
        public const string STATUS_ERROR = "error";

        /// <summary>
        /// Status of the WebAPI invoked
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Message in case there is warning/error/reminder
        /// </summary>
        public string Message { get; set; }

    }

    /// <summary>
    /// A standardized object format to be returned by the WebAPIs
    /// </summary>
    public class JSendResponse : JSendResponseBase
    {
        /// <summary>
        /// Actual content to be returned
        /// </summary>
        public object Payload { get; set; }

        public System.Web.Http.HttpError ModelState { get; set; }

        // Parameterless Constructor for XML
        protected JSendResponse()
        {
        }

        public JSendResponse(string status, string message, object payload)
        {
            Status = status;
            Message = message;
            Payload = payload;
        }

        public JSendResponse(System.Web.Http.HttpError modelState)
        {
            Status = STATUS_ERROR;
            Message = "";
            Payload = null;
            ModelState = modelState;
        }

        public JSendResponse(object payload) : this(STATUS_OK, "", payload)
        {
        }
    }

    /// <summary>
    /// JSend Response with support for jQuery DataTable serverside pagination
    /// </summary>
    /// <remarks>
    /// See https://datatables.net/manual/server-side
    /// </remarks>
    public class JQueryDatatableJSendResponse : JSendResponse
    {
        public const string JQDT_ARUGMENT_DRAW = "JQDT_ARUGMENT_DRAW";
        public const string JQDT_ARUGMENT_REC_TOTAL = "JQDT_ARUGMENT_REC_TOTAL";
        public const string JQDT_ARUGMENT_REC_FILTERED = "JQDT_ARUGMENT_REC_FILTERED";

        public JQueryDatatableJSendResponse(string status, string message, object payload) : base(status, message, payload)
        {
        }

        /// <summary>
        /// The draw counter that this object is a response to - from the draw parameter sent as part of the data request. Note that it is strongly recommended for security reasons that you cast this parameter to an integer, rather than simply echoing back to the client what it sent in the draw parameter, in order to prevent Cross Site Scripting (XSS) attacks.
        /// </summary>
        public int draw { get; set; }

        /// <summary>
        /// Total records, before filtering (i.e. the total number of records in the database)
        /// </summary>
        public int recordsTotal { get; set; }


        /// <summary>
        /// Total records, after filtering (i.e. the total number of records after filtering has been applied - not just the number of records being returned for this page of data).
        /// </summary>
        public int recordsFiltered { get; set; }
    }
}