﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;
using WeTravelWebApp.Models;

namespace WeTravelWebApp.Providers
{
    public class WebAPIActionFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Response != null)
            {
                // Wrap the response with a JSend Response
                if (actionExecutedContext.Response.Content is System.Net.Http.ObjectContent)
                {
                    var actArguments = actionExecutedContext.ActionContext.ActionArguments;
                    var oriObj = (System.Net.Http.ObjectContent)actionExecutedContext.Response.Content;
                    if (oriObj.Formatter is System.Net.Http.Formatting.JsonMediaTypeFormatter)
                    {
                        if (oriObj.Value is System.Web.Http.HttpError)
                        {
                            var httpError = oriObj.Value as System.Web.Http.HttpError;
                            var jsendObj = new JSendResponse(httpError.ModelState);
                            actionExecutedContext.Response.Content = new ObjectContent(typeof(JSendResponse), jsendObj, oriObj.Formatter);
                            return;
                        }

                        string jsendMsg = actArguments.ContainsKey(JSendResponse.ARUGMENT_MSG) ? actArguments[JSendResponse.ARUGMENT_MSG] as String : "";
                        var jsendStatus = JSendResponse.STATUS_OK;
                        if (actArguments.ContainsKey(JSendResponse.ARUGMENT_STATUS))
                        {
                            switch (actArguments[JSendResponse.ARUGMENT_STATUS] as string)
                            {
                                case JSendResponse.STATUS_OK:
                                    jsendStatus = JSendResponse.STATUS_OK;
                                    break;
                                case JSendResponse.STATUS_WARN:
                                    jsendStatus = JSendResponse.STATUS_WARN;
                                    break;
                                case JSendResponse.STATUS_ERROR:
                                    jsendStatus = JSendResponse.STATUS_ERROR;
                                    actionExecutedContext.Response.StatusCode = System.Net.HttpStatusCode.Conflict;
                                    break;
                                default:
                                    break;
                            }
                        }

                        if (actArguments.ContainsKey(JQueryDatatableJSendResponse.JQDT_ARUGMENT_REC_TOTAL))
                        {
                            var jsendObj = new JQueryDatatableJSendResponse(jsendStatus, jsendMsg, oriObj.Value)
                            {
                                draw = (int)actArguments[JQueryDatatableJSendResponse.JQDT_ARUGMENT_DRAW],
                                recordsTotal = (int)actArguments[JQueryDatatableJSendResponse.JQDT_ARUGMENT_REC_TOTAL],
                                recordsFiltered = (int)actArguments[JQueryDatatableJSendResponse.JQDT_ARUGMENT_REC_FILTERED],
                            };
                            actionExecutedContext.Response.Content = new ObjectContent(typeof(JQueryDatatableJSendResponse), jsendObj, oriObj.Formatter);
                        }
                        else
                        {
                            var jsendObj = new JSendResponse(jsendStatus, jsendMsg, oriObj.Value);
                            actionExecutedContext.Response.Content = new ObjectContent(typeof(JSendResponse), jsendObj, oriObj.Formatter);
                        }

                    }

                }
            }

            base.OnActionExecuted(actionExecutedContext);
        }
        
    }

}
