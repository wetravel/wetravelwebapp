﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using WeTravelWebApp.DAL;
using WeTravelWebApp.Exceptions;
using WeTravelWebApp.Utils;

namespace WeTravelWebApp.Controllers
{
    /// <summary>
    /// Provide Account Info Accesses
    /// </summary>
    public abstract class ApiControllerBase : ApiController
    {
        public ApiControllerBase()
        {
        }

        // For general user management
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get { return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
            private set { _userManager = value; }
        }
        
        /// <summary>
        /// Get the real User Identity
        /// </summary>
        /// <returns></returns>
        public ClaimsIdentity RecognisedIdentity
        {
            get
            {
                ClaimsIdentity cookiesIdentity = Authentication.User.Identities.SingleOrDefault(x => x.AuthenticationType == CookieAuthenticationDefaults.AuthenticationType);
                ClaimsIdentity tokenIdentity = Authentication.User.Identities.SingleOrDefault(x => x.AuthenticationType == OAuthDefaults.AuthenticationType);
                return tokenIdentity ?? cookiesIdentity;
            }
        }

        /// <summary>
        /// Get the real userID
        /// </summary>
        /// <returns></returns>
        public string RecognisedUserId
        {
            get
            {
                return RecognisedIdentity?.GetUserId();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }
        
        /// <summary>
        /// Throw exception if invalid
        /// </summary>
        /// <param name="modelState"></param>
        /// <returns></returns>
        /// <exception cref="InvalidInputModelException" />
        public void ThrowExceptionIfInvalid(ModelStateDictionary modelState)
        {
            if (!modelState.IsValid)
            {
                throw new InvalidInputModelException(GetAllErrorMessageJoined(modelState));
            }
        }

        #region Helpers

        /// <summary>
        /// Join all the error message of each field together
        /// </summary>
        /// <param name="modelState"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public string GetAllErrorMessageJoined(ModelStateDictionary modelState, string separator = ".")
        {
            //var modelStateErrMsgList = modelState.Select(x => String.Join($"{separator} ", x.Value.Errors.Select(e => e.ErrorMessage)).Replace($"{separator}{separator}", separator).TrimEnd());
            //string modelStateErrMsgJoined = String.Join($"{separator} ", modelStateErrMsgList).Replace($"{separator}{separator}", separator).TrimEnd();

            var modelStateErrMsgList = modelState.Select(x => TextUtil.JoinWithSeparator(x.Value.Errors.Select(e => e.ErrorMessage), separator));
            string modelStateErrMsgJoined = TextUtil.JoinWithSeparator(modelStateErrMsgList, separator);
            return modelStateErrMsgJoined;
        }

        protected IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        protected IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }


        #endregion
    }
}