﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WeTravelWebApp.DAL;
using WeTravelWebApp.DTO;
using WeTravelWebApp.Hubs;
using WeTravelWebApp.Models;

namespace WeTravelWebApp.Controllers
{
    /// <summary>
    /// Wrap the Geobyte Entry points
    /// </summary>
    [AllowAnonymous]
    [RoutePrefix("api/Geobytes")]
    public class GeobytesController : ApiController
    {
        /// <summary>
        /// Wrapper for AutoCompleteCity API
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="q"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("AutoCompleteCity")]
        public async Task<HttpResponseMessage> AutoCompleteCity(string callback, string q)
        {
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync($"http://gd.geobytes.com/AutoCompleteCity?callback={callback}&q={q}");
                return response;
            }
        }

        /// <summary>
        /// Wrapper for GetCityDetails API
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="fqcn"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetCityDetails")]
        public async Task<HttpResponseMessage> GetCityDetails(string callback, string fqcn)
        {
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync($"http://getcitydetails.geobytes.com/GetCityDetails?callback={callback}&fqcn={fqcn}");
                return response;
            }
        }

    }
}
