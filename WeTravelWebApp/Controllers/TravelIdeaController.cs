﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WeTravelWebApp.DAL;
using WeTravelWebApp.DTO;
using WeTravelWebApp.Hubs;
using WeTravelWebApp.Models;

namespace WeTravelWebApp.Controllers
{
    /// <summary>
    /// Manage the content of travel ideas
    /// </summary>
    [Authorize]
    [RoutePrefix("api/TravelIdea")]
    public class TravelIdeaController : ApiControllerBase
    {
        /// <summary>
        /// Search or retrieve list of travel ideas
        /// </summary>
        /// <param name="isTextOnly">Indicate whether RTF content is not preferred. This maybe useful for mobile device</param>
        /// <param name="newItemsAfter">Specify the datetime if delta is preferred instead of full-set</param>
        /// <param name="tags">Search by Tags</param>
        /// <param name="keywords">Search by keywords (separated by space)</param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public IEnumerable<TravelIdeaDTO> GetTravelIdeas(bool isTextOnly = false, DateTime? newItemsAfter = null, [FromUri]IEnumerable<string> tags = null, string keywords = "")
        {
            tags = tags ?? Enumerable.Empty<string>();
            var keywordList = keywords.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            var db = new ApplicationDbContext();
            var entities = from idea in db.TravelIdeas
                           where (newItemsAfter == null || idea.CreatedAt > newItemsAfter)
                           && (!tags.Any() || idea.Tags.Any(x => tags.Contains(x.TagKey)))
                           && !idea.IsDeleted
                           select idea;
            foreach (string keyword in keywordList)
            {
                entities = entities.Where(x
                    => x.ContentText.Contains(keyword)
                    || x.Title.Contains(keyword)
                    || x.Tags.Any(t => t.TagKey == keyword));
            }

            var resultDto = entities.Select(new TravelIdeaMapper().SelectorExpression).OrderByDescending(x => x.CreatedAt).ToList();
            if (isTextOnly)
            {
                resultDto.ForEach(x => x.ContentRTF = "");
            }
            return resultDto;
        }

        /// <summary>
        /// Retrieve an idea
        /// </summary>
        /// <param name="id">ID of the target</param>
        /// <param name="isTextOnly">Indicate whether RTF content is not preferred. This maybe useful for mobile device</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        public TravelIdeaDTO GetTravelIdea(string id, bool isTextOnly = false)
        {
            var db = new ApplicationDbContext();
            var entity = getIdeaByIdThrowException(id, db);

            var resultDto = new TravelIdeaMapper().SelectorExpression.Compile().Invoke(entity);
            if (isTextOnly)
            {
                resultDto.ContentRTF = "";
            }
            return resultDto;
        }

        /// <summary>
        /// Create a new idea
        /// </summary>
        /// <param name="dto">Body of the idea</param>
        /// <returns>Body of the idea</returns>
        [HttpPost]
        [Route("")]
        public TravelIdeaDTO CreateTravelIdea([FromBody]TravelIdeaDTO dto)
        {
            ThrowExceptionIfInvalid(ModelState);
            var db = new ApplicationDbContext();
            var user = db.Users.SingleOrDefault(x => x.Id == RecognisedUserId);
            if (user == null) { throw new UnauthorizedAccessException(); }

            var entity = db.TravelIdeas.Create();
            entity.Author = user;
            new TravelIdeaMapper().CommonMapping(entity, dto, db);

            db.TravelIdeas.Add(entity);
            db.SaveChanges(user.UserName);
            var resultDto = new TravelIdeaMapper().SelectorExpression.Compile().Invoke(entity);

            var hubContext = Microsoft.AspNet.SignalR.GlobalHost.ConnectionManager.GetHubContext<IdeaHub>();
            hubContext.Clients.All.addNewMessageToPage(resultDto.Title, resultDto.ContentText);
            hubContext.Clients.All.newIdea(resultDto);
            return resultDto;
        }

        /// <summary>
        /// Update a travel idea
        /// </summary>
        /// <param name="id">ID of the target</param>
        /// <param name="dto">Body of the idea</param>
        /// <returns>Body of the idea</returns>
        [HttpPost]
        [Route("{id}")]
        public TravelIdeaDTO UpdateTravelIdea([FromUri]string id, [FromBody]TravelIdeaDTO dto)
        {
            ThrowExceptionIfInvalid(ModelState);
            var db = new ApplicationDbContext();
            var user = db.Users.SingleOrDefault(x => x.Id == RecognisedUserId);
            if (user == null) { throw new UnauthorizedAccessException(); }

            var entity = getIdeaByIdThrowException(id, db);
            if (entity.Author.Id != user.Id)
            {
                throw new UnauthorizedAccessException($"This idea can only be edited by the author {user.UserName}");
            }

            new TravelIdeaMapper().CommonMapping(entity, dto, db);

            db.SaveChanges(user.UserName);
            var resultDto = new TravelIdeaMapper().SelectorExpression.Compile().Invoke(entity);
            return resultDto;
        }

        /// <summary>
        /// Mark an idea as deleted
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete]
        [Route("")]
        public object Delete(string id)
        {
            var db = new ApplicationDbContext();
            var entity = getIdeaByIdThrowException(id, db);

            entity.IsDeleted = true;
            db.SaveChanges(User.Identity.Name);

            return null;
        }


        /// <summary>
        /// Search or retrieve comments of a travel ideas
        /// </summary>
        /// <param name="id">ID of the idea</param
        /// <param name="tags">Search by Tags</param>>
        /// <param name="tags">Search by Tags</param>
        /// <param name="keywords">Search by keywords (separated by space)</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}/Comments")]
        public IEnumerable<TravelIdeaCommentDTO> GetComments(string id, DateTime? newItemsAfter = null, [FromUri]IEnumerable<string> tags = null, string keywords = "")
        {
            tags = tags ?? Enumerable.Empty<string>();
            var keywordList = keywords.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            var db = new ApplicationDbContext();
            var idea = getIdeaByIdThrowException(id, db);
            var entities = from comment in db.TravelIdeaComments
                           where (comment.TravelIdea != null && comment.TravelIdea.Id == idea.Id)
                           && (newItemsAfter == null || comment.CreatedAt > newItemsAfter)
                           && (!tags.Any() || comment.Tags.Any(x => tags.Contains(x.TagKey)))
                           && !comment.IsDeleted
                           select comment;
            foreach (string keyword in keywordList)
            {
                entities = entities.Where(x => x.ContentText.Contains(keyword));
            }

            var resultDto = entities.Select(new TravelIdeaCommentMapper().SelectorExpression).OrderByDescending(x => x.CreatedAt).ToList();
            return resultDto;
        }

        /// <summary>
        /// Create a new comment
        /// </summary>
        /// <param name="dto">Body of the idea</param>
        /// <returns>Body of the idea</returns>
        [HttpPost]
        [Route("{id}/Comments")]
        public TravelIdeaCommentDTO PostNewComment([FromUri]string id, [FromBody]TravelIdeaCommentDTO dto)
        {
            ThrowExceptionIfInvalid(ModelState);
            var db = new ApplicationDbContext();
            var user = db.Users.SingleOrDefault(x => x.Id == RecognisedUserId);
            if (user == null) { throw new UnauthorizedAccessException(); }
            
            var idea = getIdeaByIdThrowException(id, db);
            var entity = db.TravelIdeaComments.Create();
            entity.Author = user;
            entity.ContentText = dto.ContentText;


            var distinctTags = dto.Tags.Distinct();
            var existingTags = from tag in db.Tags
                               where distinctTags.Contains(tag.TagKey)
                               select tag;
            var newTagKeys = distinctTags.Where(x => !existingTags.Any(y => y.TagKey == x));
            var newTags = newTagKeys.Select(x => new Tag() { TagKey = x });
            var finalTagList = new List<Tag>(existingTags);
            foreach (var finalTag in finalTagList)
            {
                finalTag.TaggableArticles.Add(entity);
            }
            finalTagList.AddRange(newTags);
            entity.Tags = finalTagList;

            entity.TravelIdea = idea;
            idea.TravelIdeaComments.Add(entity);
            db.SaveChanges(user.UserName);
            var resultDto = new TravelIdeaCommentMapper().SelectorExpression.Compile().Invoke(entity);

            var hubContext = Microsoft.AspNet.SignalR.GlobalHost.ConnectionManager.GetHubContext<IdeaHub>();
            hubContext.Clients.All.newIdeaComment(resultDto);
            return resultDto;
        }

        #region Helper
        /// <summary>
        /// Get the idea by ID
        /// </summary>
        /// <exception cref="Exceptions.RecordNotFoundException" />
        /// <param name="id"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        TravelIdea getIdeaByIdThrowException(string id, ApplicationDbContext db)
        {
            var guid = Guid.Parse(id);
            var entity = db.TravelIdeas.FirstOrDefault(x => x.Id == guid && !x.IsDeleted);
            if (entity == null)
            {
                throw new Exceptions.RecordNotFoundException("Your preferred idea is not found");
            }
            return entity;
        }
        #endregion
    }
}
