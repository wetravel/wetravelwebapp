﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WeTravelWebApp.Controllers
{
    /// <summary>
    /// For Connectivity test only
    /// </summary>
    [Authorize]
    public class ValuesController : ApiController
    {
        /// <summary>
        /// For Connectivity test only
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        /// <summary>
        /// For Connectivity test only
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string Get(int id)
        {
            return "value";
        }

        /// <summary>
        /// For Connectivity test only
        /// </summary>
        /// <param name="value"></param>
        public void Post([FromBody]string value)
        {
        }

        /// <summary>
        /// For Connectivity test only
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        public void Put(int id, [FromBody]string value)
        {
        }

        /// <summary>
        /// For Connectivity test only
        /// </summary>
        /// <param name="id"></param>
        public void Delete(int id)
        {
        }
    }
}
