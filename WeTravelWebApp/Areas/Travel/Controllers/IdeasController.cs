﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WeTravelWebApp.DAL;
using WeTravelWebApp.Models;

namespace WeTravelWebApp.Areas.Travel.Controllers
{
    public class IdeasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult SignalRDemo()
        {
            return View();
        }

        // GET: Travel/Ideas
        public async Task<ActionResult> Index()
        {
            return View(await db.TravelIdeas.ToListAsync());
        }

        // GET: Travel/Ideas/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TravelIdea travelIdea = await db.TravelIdeas.FindAsync(id);
            if (travelIdea == null)
            {
                return HttpNotFound();
            }
            return View(travelIdea);
        }

        // GET: Travel/Ideas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Travel/Ideas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "TaggableArticleId,IsDeleted,CreatedBy,CreatedAt,UpdatedBy,UpdatedAt,Id,Title,Destination,From,To,ContentText,ContentRTF")] TravelIdea travelIdea)
        {
            if (ModelState.IsValid)
            {
                travelIdea.TaggableArticleId = Guid.NewGuid();
                db.TravelIdeas.Add(travelIdea);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(travelIdea);
        }

        // GET: Travel/Ideas/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TravelIdea travelIdea = await db.TravelIdeas.FindAsync(id);
            if (travelIdea == null)
            {
                return HttpNotFound();
            }
            return View(travelIdea);
        }

        // POST: Travel/Ideas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "TaggableArticleId,IsDeleted,CreatedBy,CreatedAt,UpdatedBy,UpdatedAt,Id,Title,Destination,From,To,ContentText,ContentRTF")] TravelIdea travelIdea)
        {
            if (ModelState.IsValid)
            {
                db.Entry(travelIdea).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(travelIdea);
        }

        // GET: Travel/Ideas/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TravelIdea travelIdea = await db.TravelIdeas.FindAsync(id);
            if (travelIdea == null)
            {
                return HttpNotFound();
            }
            return View(travelIdea);
        }

        // POST: Travel/Ideas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            TravelIdea travelIdea = await db.TravelIdeas.FindAsync(id);
            db.TravelIdeas.Remove(travelIdea);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
