﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeTravelWebApp.Utils
{
    public static class TextUtil
    {
        /// <summary>
        /// Combine the root URI with relative URI. The returned new uri will be {rootUri}/{relativeUri}
        /// </summary>
        /// <param name="rootUri"></param>
        /// <param name="relativeUri"></param>
        /// <returns></returns>
        public static string UriPathCombine(this string rootUri, string relativeUri)
        {
            if (String.IsNullOrWhiteSpace(relativeUri))
            {
                return rootUri;
            }
            else if (String.IsNullOrWhiteSpace(rootUri))
            {
                return relativeUri;
            }
            return $"{rootUri.TrimEnd('/')}/{relativeUri.Trim('/')}";
        }

        /// <summary>
        /// Join msg with separator
        /// </summary>
        /// <param name="textList"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static string JoinWithSeparator(this IEnumerable<string> textList, string separator)
        {
            return String.Join($"{separator} ", textList).Replace($"{separator}{separator}", separator).TrimEnd();
        }

        /// <summary>
        /// Return Default value if text is null or whitespace
        /// </summary>
        /// <param name="text"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string DefaultIfWhitespace(this string text, string defaultValue)
        {
            return String.IsNullOrWhiteSpace(text) ? defaultValue : text;
        }

        /// <summary>
        /// Support String.compare with different StringComparison
        /// </summary>
        /// <remarks>See http://stackoverflow.com/a/17563994/4684232</remarks>
        /// <param name="source"></param>
        /// <param name="toCheck"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source != null && toCheck != null && source.IndexOf(toCheck, comp) >= 0;
        }

    }
}