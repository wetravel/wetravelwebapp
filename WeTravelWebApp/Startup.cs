﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(WeTravelWebApp.Startup))]

namespace WeTravelWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Enable CORS. See http://stackoverflow.com/a/32294458/4684232
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            ConfigureAuth(app);
        }
    }
}
