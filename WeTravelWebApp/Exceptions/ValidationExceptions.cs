﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WeTravelWebApp.Exceptions
{
    /// <summary>
    /// Indicate that the exception type is custom validation exception (i.e. not critical)
    /// </summary>
    public interface ICustomValidationException { }

    /// <summary>
    /// For the case that input param/DTO/Model from web request is invalid
    /// </summary>
    [System.Serializable]
    public class InvalidInputModelException : SimplifiedException, ICustomValidationException
    {
        public InvalidInputModelException(string message) : base(message) { }
        
    }
    
    /// <summary>
    /// For the case that input param/DTO/Model from web request is violating business rules
    /// </summary>
    [System.Serializable]
    public class BusinessRuleViolateException : SimplifiedException, ICustomValidationException
    {
        public BusinessRuleViolateException(string message) : base(message) { }

    }

    /// <summary>
    /// For the case that input param/DTO/Model from web request is pointing to non-existing record
    /// </summary>
    [System.Serializable]
    public class RecordNotFoundException : SimplifiedException, ICustomValidationException
    {
        public RecordNotFoundException(string message) : base(message) { }

    }
    
}
