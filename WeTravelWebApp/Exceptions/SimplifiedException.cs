﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WeTravelWebApp.Exceptions
{
    /// <summary>
    /// Customized Outbound Exception with StackTrace details hidden, thus protecting against exposure of system structure
    /// </summary>
    [System.Serializable]
    public class SimplifiedException : Exception
    {
        public SimplifiedException(string message) : base(message) { }

        public override string StackTrace
        {
            get
            {
                //return base.StackTrace;
                return "";
            }
        }
    }
}
